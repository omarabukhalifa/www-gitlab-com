---
layout: handbook-page-toc
title: "Security Awards Leaderboard"
---

### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This page is [auto-generated and updated every Mondays](../security-awards-program.html#process).

# Leaderboard FY23

## Yearly

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@leipert](https://gitlab.com/leipert) | 1 | 500 |
| [@georgekoltsov](https://gitlab.com/georgekoltsov) | 2 | 300 |
| [@10io](https://gitlab.com/10io) | 3 | 100 |
| [@mwoolf](https://gitlab.com/mwoolf) | 4 | 60 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@katiemacoy](https://gitlab.com/katiemacoy) | 1 | 50 |

### Non-Engineering

Category is empty

### Community

Category is empty

## FY23-Q1

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@leipert](https://gitlab.com/leipert) | 1 | 500 |
| [@georgekoltsov](https://gitlab.com/georgekoltsov) | 2 | 300 |
| [@10io](https://gitlab.com/10io) | 3 | 100 |
| [@mwoolf](https://gitlab.com/mwoolf) | 4 | 60 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@katiemacoy](https://gitlab.com/katiemacoy) | 1 | 50 |

### Non-Engineering

Category is empty

### Community

Category is empty


